<?php

namespace Nekofar\Nomics\Tests;


use Http\Client\Common\Exception\ClientErrorException;
use Http\Client\Exception;
use JsonMapper_Exception;
use Nekofar\Nomics\Client;
use Nekofar\Nomics\Config;
use Nekofar\Nomics\HttpClient\HttpMethodsClientFactory;
use Nekofar\Nomics\Model\Ticker;
use PHPUnit_Framework_Constraint_IsType as IsType;
use Throwable;

class ClientTest extends TestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * HttpMethodsClient
     */
    private $httpClient;

    /**
     *
     */
    public function testCreate()
    {
        $config = Config::create('b69315e440beb145');
        $client = Client::create($config);

        $this->assertInstanceOf(Client::class, $client);
    }

    /**
     *
     * @throws Exception
     * @throws JsonMapper_Exception
     * @throws Throwable
     */
    public function testGetTickersFailure()
    {
        $this->expectException(ClientErrorException::class);
        $this->expectExceptionMessage('Unauthorized');

        $this->setHttpResponse('GetTickersFailure.txt');

        $this->client->getTickers();
    }

    /**
     *
     * @throws Exception
     * @throws JsonMapper_Exception
     * @throws Throwable
     */
    public function testGetTickersSuccess()
    {
        $this->setHttpResponse('GetTickersSuccess.txt');

        $result = $this->client->getTickers();

        $this->assertInternalType(IsType::TYPE_ARRAY, $result);
        $this->assertContainsOnlyInstancesOf(Ticker::class, $result);
    }

    /**
     *
     */
    protected function setUp()
    {
        parent::setUp();

        $this->httpClient = HttpMethodsClientFactory::create([], $this->getHttpClient());

        $this->client = new Client($this->httpClient, $this->getJsonMapper());
    }


}
