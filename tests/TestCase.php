<?php


namespace Nekofar\Nomics\Tests;


use Http\Client\HttpClient;
use Http\Mock\Client;
use JsonMapper;
use Psr\Http\Message\ResponseInterface;
use ReflectionObject;
use function GuzzleHttp\Psr7\parse_response;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var JsonMapper
     */
    private $jsonMapper;

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        if (null === $this->httpClient) {
            $this->httpClient = new Client();
        }

        return $this->httpClient;
    }

    /**
     * @param $path
     */
    public function setHttpResponse($path)
    {
        $this->httpClient->addResponse($this->getHttpResponse($path));
    }

    /**
     * @param $path
     * @return ResponseInterface
     */
    public function getHttpResponse($path)
    {
        if ($path instanceof ResponseInterface) {
            return $path;
        }

        $ref = new ReflectionObject($this);
        $dir = dirname($ref->getFileName());

        // if mock file doesn't exist, check parent directory
        if (!file_exists($dir . '/Mock/' . $path) && file_exists($dir . '/../Mock/' . $path)) {
            return parse_response(file_get_contents($dir . '/../Mock/' . $path));
        }

        return parse_response(file_get_contents($dir . '/Mock/' . $path));
    }

    /**
     * @return JsonMapper
     */
    public function getJsonMapper()
    {
        if (null === $this->jsonMapper) {
            $this->jsonMapper = new JsonMapper();
        }

        return $this->jsonMapper;
    }
}