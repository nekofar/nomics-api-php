<?php
/**
 * @package Nekofar\Nomics
 *
 * @author Milad Nekofar <milad@nekofar.com>
 */

namespace Nekofar\Nomics\Client;

use Nekofar\Nomics\Model\Ticker;
use Throwable;
use function nspl\a\filter;
use function nspl\args\expects;
use const nspl\args\array_;
use const nspl\args\string;

/**
 * Trait TickerTrait
 */
trait TickerTrait
{
    /**
     * @param array $args
     * @return Ticker[]
     *
     * @throws Throwable
     */
    public function getTickers(array $args = [])
    {
        // Merge default data with the given args.
        $args = array_merge([
            'ids' => [],
            'interval' => [],
            'convert' => ''
        ], $args);

        // Some validation on args data.
        expects([array_], $args['ids']);
        expects([array_], $args['interval']);
        expects([string], $args['convert']);

        // Prepare data and filter empty args.
        $data = filter('nspl\args\_p\isNotEmpty', [
            'ids' => implode(',', $args['ids']),
            'interval' => implode(',', $args['interval']),
            'convert' => $args['convert']
        ]);

        // Build data query using args.
        $data = http_build_query($data, null, '&');

        // Send a get request to the endpoint with data
        $resp = $this->httpClient->get('/currencies/ticker?' . $data);

        // Convert response body from json string.
        $json = json_decode($resp->getBody());

        // Map json response object to model
        return $this->jsonMapper->mapArray($json, [], Ticker::class);
    }
}
