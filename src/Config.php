<?php
/**
 * @package Nekofar\Nomics
 *
 * @author Milad Nekofar <milad@nekofar.com>
 */

namespace Nekofar\Nomics;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Message\Authentication;
use Http\Message\Authentication\QueryParam;
use JsonMapper;
use Nekofar\Nomics\HttpClient\HttpMethodsClientFactory;

/**
 * Class Config
 */
class Config
{
    /**
     * @const string
     */
    const CLIENT_NAME = 'Nekofar Nomics';

    /**
     * @const string
     */
    const CLIENT_VERSION = '1.0.0';

    /**
     * @const string
     */
    const CLIENT_BASE_URL = 'https://api.nomics.com/v1/';

    /**
     * @var Authentication
     */
    private $authentication;

    /**
     * Config constructor.
     * @param Authentication $authentication
     */
    public function __construct(Authentication $authentication)
    {
        $this->authentication = $authentication;
    }

    /**
     * @param string $apiKey
     *
     * @return Config
     */
    public static function create($apiKey)
    {
        return new static(new QueryParam(['key' => $apiKey]));
    }

    /**
     * @return HttpMethodsClient
     */
    public function createHttpClient()
    {
        // Authenticate requests sent through the client.
        $plugins[] = new AuthenticationPlugin($this->authentication);

        $httpClient = HttpMethodsClientFactory::create($plugins);

        return $httpClient;
    }

    /**
     * @return JsonMapper
     */
    public function createJsonMapper()
    {
        $jsonMapper = new JsonMapper();

        return $jsonMapper;
    }
}
