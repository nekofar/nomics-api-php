<?php
/**
 * @package Nekofar\Nomics
 *
 * @author Milad Nekofar <milad@nekofar.com>
 */

namespace Nekofar\Nomics\Model;

/**
 * Class Ticker
 */
class Ticker
{
    /**
     * @var string
     */
    public $currency;

    /**
     * @var string
     */
    public $id;

    /**
     * @var float
     */
    public $price;

    /**
     * @var \DateTime
     */
    public $priceDate;

    /**
     * @var string
     */
    public $symbol;

    /**
     * @var float
     */
    public $circulatingSupply;

    /**
     * @var float
     */
    public $maxSupply;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $logoUrl;

    /**
     * @var float
     */
    public $marketCap;

    /**
     * @var integer
     */
    public $rank;

    /**
     * @var float
     */
    public $high;

    /**
     * @var \DateTime
     */
    public $highTimestamp;

    /**
     * @param string $currency
     *
     * @return void
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param string $id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param float $price
     *
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param \DateTime $priceDate
     *
     * @return void
     */
    public function setPriceDate(\DateTime $priceDate)
    {
        $this->priceDate = $priceDate;
    }

    /**
     * @param string $symbol
     *
     * @return void
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @param float $circulatingSupply
     *
     * @return void
     */
    public function setCirculatingSupply($circulatingSupply)
    {
        $this->circulatingSupply = $circulatingSupply;
    }

    /**
     * @param float $maxSupply
     *
     * @return void
     */
    public function setMaxSupply($maxSupply)
    {
        $this->maxSupply = $maxSupply;
    }

    /**
     * @param string $name
     *
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $logoUrl
     *
     * @return void
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;
    }

    /**
     * @param float $marketCap
     *
     * @return void
     */
    public function setMarketCap($marketCap)
    {
        $this->marketCap = $marketCap;
    }

    /**
     * @param integer $rank
     *
     * @return void
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * @param float $high
     *
     * @return void
     */
    public function setHigh($high)
    {
        $this->high = $high;
    }

    /**
     * @param \DateTime $highTimestamp
     *
     * @return void
     */
    public function setHighTimestamp(\DateTime $highTimestamp)
    {
        $this->highTimestamp = $highTimestamp;
    }
}
