# Nomics PHP API

[![Packagist Version](https://img.shields.io/packagist/v/nekofar/nomics.svg)][1]
[![PHP from Packagist](https://img.shields.io/packagist/php-v/nekofar/nomics.svg)][1]
[![Travis (.com) branch](https://img.shields.io/travis/com/nekofar/nomics-api-php/master.svg)][3]
[![Codecov](https://img.shields.io/codecov/c/gh/nekofar/nomics-api-php.svg)][4]
[![Packagist](https://img.shields.io/packagist/l/nekofar/nomics.svg)][2]
[![Twitter: nekofar](https://img.shields.io/twitter/follow/nekofar.svg?style=flat)][7]

> This is a PHP wrapper for the [Nomics API][6].

## Installation

This wrapper relies on HTTPlug, which defines how HTTP message should be sent and received. 
You can use any library to send HTTP messages that implements [php-http/client-implementation][5].

```bash
composer require nekofar/nomics:^1.0@dev
```

To install with cURL you may run the following command:

```bash
composer require nekofar/nomics:^1.0@dev php-http/curl-client:^1.0
```

## Usage

Use your api key to access your own account.

```php
use \Nekofar\Nomics\Client;
use \Nekofar\Nomics\Config;

$config = Config::create('apiKey')
$client = Client::create($config)

try {
    $tickers = $client->getTickers([
        ids => ['BTC', 'ETH', 'XRP']
    ]);

    print_r($tickers);

} catche (\Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

## Contributing

The test suite is built using PHPUnit. Run the suite of unit tests by running
the `phpunit` command or this composer script.

```bash
composer test
```

---
[1]: https://packagist.org/packages/nekofar/nomics
[2]: https://github.com/nekofar/nomics-api-php/blob/master/LICENSE
[3]: https://travis-ci.com/nekofar/nomics-api-php
[4]: https://codecov.io/gh/nekofar/nomics-api-php
[5]: https://packagist.org/providers/php-http/client-implementation
[6]: https://docs.nomics.com/
[7]: https://twitter.com/nekofar
